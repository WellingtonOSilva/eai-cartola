from twilio.twiml.messaging_response import MessagingResponse
import helpers

def mercado(api):
    mercado = api.mercado()
    quote = f'{helpers.replyInfo()} \n\n' \
            f'Status do mercado: {mercado.status[1]} \n' \
            f'Rodada atual: {mercado.rodada_atual} \n' \
            f'Aviso: {mercado.aviso} \n' \
            f'Fechamento: {mercado.fechamento} \n'
    return msg.body(quote)
