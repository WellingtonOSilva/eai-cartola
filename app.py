from flask import Flask
import cartolafc
from twilio.rest import Client
from cartola import escalar_peso_medias
from cartola import escalar_peso_saldo_clube
import helpers
from flask import request
import logging
import random
from cartola import crawler
from twilio.twiml.messaging_response import MessagingResponse

app = Flask(__name__)

@app.route('/bot', methods=['POST'])
def bot():
    incoming_msg = request.values.get('Body', '').lower()
    resp = MessagingResponse()
    msg = resp.message()
    responded = False

    if incoming_msg in ['oi', 'ola', 'eai']:
        msg.body(greetings())
        responded = True

    if 'meu time' in incoming_msg:
        msg.body(meu_time())
        responded = True

    if 'mercado' in incoming_msg:
        msg.body(mercado())
        responded = True

    if any(item in incoming_msg.split() for item in ['escalação', 'escalacao']):
        escalacao_info()
        responded = True

    if 'liga' in incoming_msg:
        msg.body(pesquisar_liga(incoming_msg))
        responded = True

    if any(item in incoming_msg.split() for item in ['escale', 'escalacao']):
        msg.body(escalar_time(incoming_msg))
        responded = True

    if not responded:
        msg.body('Desculpe, não consegui entender')
    return str(resp)


def escalar_time(incoming_msg):

    words = incoming_msg.split()
    if any(item in words for item in ['pontos']):
        escalar('pontos')
        quote = "Time escalado"
    elif any(item in words for item in ['cartoletas']):
        escalar('cartoletas')
        quote = "Time escalado"
    else:
        quote = "Opção inválida"

    return quote


def greetings():
    return (f'{helpers.greeting()}, Sou o seu assistente do cartola. \n\n'
             'Escolha uma das opções abaixo: \n\n'
             'Digite *meu time* para ter informações \n'
             'Digite *mercado* para ter informações \n'
             'Digite *liga* {nome da liga} para ter informações \n'
             'Digite *escale* {tipo da escalação} (cartoletas ou pontos) \n'
             'Digite *escalação* para ter informações')

def meu_time():

    time = api.time_logado()
    quote = f'{helpers.replyInfo()} \n\n' \
            f'Cartoletas: {time.patrimonio} \n' \
            f'Ultima pontuação: {time.ultima_pontuacao} \n' \
            f'Valor da escalação atual: {time.valor_time} \n'

    return quote

def mercado():
    resp = MessagingResponse()
    msg = resp.message()
    mercado = api.mercado()
    quote = f'{helpers.replyInfo()} \n\n' \
            f'Status do mercado: {mercado.status[1]} \n' \
            f'Rodada atual: {mercado.rodada_atual} \n' \
            f'Aviso: {mercado.aviso} \n' \
            f'Fechamento: {mercado.fechamento} \n'
    return quote

def escalacao_info():
    quote = f'Escalacao: \n\n'
    mercado = api.mercado()

    try:
        atletas = api.time_logado().atletas

        for atleta in atletas:
            if atleta.is_capitao:
                atleta.apelido = atleta.apelido + ' - ©'

            if mercado.status[0] != 1:
                p = api.parciais().get(atleta.id, False)
                atleta_str = '*{0}*\n Status: {1}\nPontuação: {2}\n\n'.format(atleta.apelido, atleta.status[1], p.pontos)
            else:
                atleta_str = '*{0}*\nPontuação: -\nStatus: {1}\nNão jogou ainda\n\n'.format(atleta.apelido, atleta.status[1])

            quote += atleta_str
    except Exception as e:
        quote = e
        logging.error(e)
    client = Client(request.values.get('AccountSid'), '547c05961fc79a7e47da129aae54b40b')

    message = client.messages.create(
        body=quote,
        from_=request.values.get('To'),
        to=request.values.get('From')
    )

def pesquisar_liga(incoming_msg):

    words = incoming_msg.split()
    search_term = words[words.index('liga') + 1]
    liga = api.liga(search_term)

    quote = f'*Classificação da liga {liga.nome}*\n\n'

    for time in liga.times:
        atleta_str = '{0} ({1}) com *{2}* pontos\n'.format(time.nome, time.nome_cartola, time.pontos)
        quote += atleta_str

    return quote


@app.route('/diversos', methods=['GET'])
def diversos():
    time =  api.time_logado()
    return helpers.toJson(time)

@app.route('/load', methods=['GET'])
def load():
    crawler.main()
    return {"status": "true"}

@app.route('/escalar', methods=['GET'])
def escalar(tipo):
    time = api.time_logado()

    params = [3, time.patrimonio, 2, 2, 3, 3]

    escalador = escalar_peso_medias.main(*params) if tipo == 'pontos' else escalar_peso_saldo_clube.main(*params)

    atletas_ids = escalador.get_escalados_ids()

    escalacao = { "esquema": 3,
                  "atletas": atletas_ids,
                  "capitao": random.choice(atletas_ids)
                  }

    return api.escalar(escalacao)



if __name__ == '__main__':
    api = cartolafc.Api(email='wellington.spwos@outlook.com', password='24240909', attempts=5)
    app.run(port=5009)
