import json
import datetime
import random


def toJson(obj):
    return json.dumps(obj.__dict__, default=lambda o: o.strftime("%Y-%m-%d %H:%M:%S") if isinstance(o, datetime.datetime) else o.__dict__, indent=4)



def greeting():
    msgs = [
        'Eai Wellington, beleza? ',
        'Opa Wellington, tranquilo? ',
        'Salve Wellington! '
    ]
    return random.choice(msgs)

def replyInfo():
    msgs = [
        'Certo, segue o que você pediu: ',
        'Tá na mão: ',
        'Encontrei isso aqui: '
    ]
    return random.choice(msgs)
